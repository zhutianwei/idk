index_page = Vue.component('index-page',{
    data: function(){
        return { is_login: isLogin()}
    },
    methods:{
    	
    },

    
    template:` 
<div>
    <div class="jumbotron">
        <div class="container">
            <div class="row">
                <div class="col-md-6 index-img">
                    <img src="img/index_img.jpg" alt="">
                </div>
                <div class="col-md-6 text-center">
                    <p style="text-align:left;">Users can choose to be students and teachers. Teachers can change homework for students, students upload their homework, and then set a price range within which the teacher bids, and then wait for the students to choose the teacher. Then both sides can evaluate. Students can also search for the teacher on the website, send a question to the teacher and then a price to see if the teacher accepts it or not. Teachers can use what they are good at searching for students.</p>
                    <div v-if='is_login'>
                        <p><router-link to="/student" class="btn btn-default" href="" role="button">want to post questions</router-link></p>
                        <p><router-link to="/teacher" class="btn btn-default"  role="button">want to help and get reward</router-link></p> 
                    </div>
                    <div v-else='is_login'>
                        <p><router-link to="/reg" class="btn btn-default" href="" role="button">want to post questions</router-link></p>
                        <p><router-link to="/reg" class="btn btn-default"  role="button">want to help and get reward</router-link></p> 
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
          <!-- Example row of columns -->
          <div class="row">
            <div class="col-md-6">
              <img src="img/black_avater.jpg" alt="">
              <img src="img/black_wechat.jpg" alt="">        
            </div>
            <div class="col-md-6">
              <img src="img/avater.jpg" alt="">
              <img src="img/white_wechat.jpg" alt="">
           </div>
           
          </div>
    </div>
    <hr/>
    <div class="container">
    <!-- Example row of columns -->
	    <div class="row col4-img">
	      <div class="col-md-4">
	        <img src="img/comminication.jpg" alt="">
	        <p>exchanged views on a question.<br>Teachers and students communicate with each other on a question.

Students are not satisfied with the teacher's answer, can not accept the answer, at this time the teacher can also submit the answer many times, until students are satisfied with it.</p>	        
	      </div>
	      <div class="col-md-4">
	        <img src="img/select1.jpg" alt="">
	        <p>There are multiple choices.<br>

After the student submits the question, many teachers bid, at this time, the student has many choices, finally selects one of the teachers. Then the teacher began to answer.</p>
	        
	     </div>
	     <div class="col-md-4">
	        <img src="img/earnings.jpg" alt="">
	        <p>Earnings<br>

Teachers and students can make money on the system, and the students have to pay the set amount to the teacher after the teacher answers the student's questions.</p> 
	      </div>
	    </div>
    </div>
</div>

    `
})