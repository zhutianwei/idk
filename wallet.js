wallet_item = Vue.component('wallet-item', {
	data:function(){
		return {
			orderNo: 1,
			title : 12,
			status: 12,
			comment: "good job",
			rewards: 12,
		}

	},
	mounted:function(){
		console.log(this.item)
		this.orderNo = this.item.id
		this.status = this.item.status
		this.comment = "good job"

		api_get("posts", this.item.post_id, (response) => {
            this.post_data = response.data
			this.title = this.post_data.title
			for (i=0; i<this.post_data.bids.length; i++){
				if(this.post_data.bids[i].id == this.item.bid_id){
					this.rewards = this.post_data.bids[i].quote
					this.$emit("add_reward", this.rewards)
				}
			}
        })
	},
	props:{
		item:{
			type: Object,
			require: true
		}
	},
	template: `
			<tr>
					<td>{{orderNo}}</td>
					<td>{{title}}</td>
					<td>{{status}}</td>
					
					<td>{{rewards}}</td>
					<td>{{comment}}</td>
					<td class="star-bar">				
						<span class="star" title="很不好"></span>
						<span class="star" title="不好"></span>
						<span class="nostar" title="一般"></span>
						<span class="nostar" title="好"></span>
						<span class="nostar" title="很好"></span>
					</td>
			</tr>
	`
})
wallet = Vue.component('wallet', {
    data: function(){
        return {
            orderNo: "question_title",
	        title: "question quesf",
	        status: "quates ",
	        student: "Jomn",
	        rewards: 0,
			comment:"very good!",
			items:[]
        }
    },
    mounted:function(){
		rating.init('.star-bar',2);
		user_id = readCookie('user_id')
		axios.get("/orders/?bider_id="+user_id+"&status=finish").then(
			(response) => {
				this.items = response.data
			}
		)

    },
    methods: {
        reg: function(e){

		},
		add_reward: function(reward){
			this.rewards = this.rewards + reward
		}
    },

    template:`
        <div id="box">
			<label>My Earning：</label>
			<div class="money-menu">
				<div class="money-item">
				   <p>Total Earnings:</p>
				   <p>$ {{rewards}}</p>
				</div>
				<div class="money-item">
				   <p>pending Earnings:</p>
				   <p>$0</p>
				</div>
				<div class="money-item">
				   <p>Available Balance:</p>
				   <p>$0.00</p>
				</div>
				<div class="money-item">
				   <p>Total CashBack:</p>
				   <p>$0.00</p>
				</div>
			</div>
			<label>Earning list：</label>
			<table border="" cellspacing="" cellpadding="">
				<tr>
					<th>Order No</th>
					<th>Title</th>
					<th>Status</th>
					<th>Rewards</th>
					<th>comments</th>
					<th>Rate</th>
				</tr>
				<wallet-item v-for="item in items" v-on:add_reward="add_reward" v-bind:item="item"></wallet-item>
			</table>
			
		</div>
    `
})
var rating=(function(){
var init=function(el,num){	
	var $starBar=$(el);
	var $itemstar=$starBar.find('.nostar');
	var lightOn=function(num){
	  $itemstar.each(function(index){
	  if(index<num+1){
		  $(this).css('background-image','url(img/red_star.png)');	
	  }
	  else{
    	$(this).css('background-image','url(img/gray_star.png)');
	  }
	})
	};
	$('.star-bar')
//	.on("mouseover",".nostar",function(){
//		console.log("mouseover in num " + num);
//		lightOn($(this).index()+1);
//	})
	.on("click",".nostar",function(){
		num=$(this).index()+1;
		lightOn(num);
    console.log("click in num " + num);
	})
	.on("click",".star",function(){
		num=$(this).index()+1;
		lightOn(num);
    console.log("click in num " + num);
	})
//	.on("mouseout",".nostar",function(){
//		lightOn(num);
//	});
	}
	return {
	init:init	
	};
})();
