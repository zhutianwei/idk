avater = Vue.component('avater',{
    data: function(){
        
        return {
            avater_id:1,
            avater_url: ""
        }
    },
    watch: {
        'user_id' (to, from) {
            axios.get("/profile/"+ to +"/").then(
                (repsonse) =>{
                   this.avater_id = repsonse.data.avater_id
                   this.avater_url = "/static/img/avater_"+this.avater_id+".jpg"
                }
            )
        }
    },
    mounted:function(){
         console.log("mounted avater")
        if (this.user_id){
           
            axios.get("/profile/"+this.user_id+"/").then(
                (repsonse) =>{
                   this.avater_id = repsonse.data.avater_id
                   this.avater_url = "/static/img/avater_"+this.avater_id+".jpg"
                }
            )
        } else if(this.avater_raw_id){
              this.avater_id = this.avater_raw_id
              this.avater_url = "/static/img/avater_"+this.avater_id+".jpg"
        }
    },

    props: {
        user_id: {
            type:Number,
            require:false,
        },
        avater_raw_id:Number

    },
    template: `
         　 <img v-bind:src="avater_url"></img>
    `,
})